<?php

namespace App\Service;

use App\Helper\Mailable;
use Swift_Mailer;
use Swift_Message;

class EMailer
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(Mailable $mailable): bool
    {
        $message = (new Swift_Message($mailable->getSubject()))
            ->setBody($mailable->getMessageContent())
            ->setFrom($mailable->getFrom())
            ->setTo($mailable->getDestination());

        if ($this->mailer->send($message) !== 0) return true;
        return false;
    }
}