<?php

namespace App\Service;

use App\Entity\MailJoke;
use App\Helper\LoggerTrait;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LogLevel;

class DataSaver
{
    use LoggerTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ICdnService
     */
    private $ICdnService;

    public function __construct(EntityManagerInterface $em, ICdnService $ICdnService)
    {
        $this->em = $em;
        $this->ICdnService = $ICdnService;
    }

    public function saveData(MailJoke $joke)
    {
        try {
            $content = $this->ICdnService->getJoke($joke->getCategory());
            $joke->setContent($content);

            $this->em->persist($joke);
            $this->em->flush();

            return $joke;
        } catch (Exception $e) {
            $this->logMessage(LogLevel::ERROR, $e->getMessage(), [
                __METHOD__ => $joke
            ]);

            return null;
        }
    }
}