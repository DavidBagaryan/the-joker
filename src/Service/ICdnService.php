<?php

namespace App\Service;

use App\Helper\LoggerTrait;
use Exception;
use GuzzleHttp\Exception\BadResponseException;
use Http\Client\HttpAsyncClient;
use Http\Message\RequestFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use stdClass;
use UnexpectedValueException;

class ICdnService
{
    use LoggerTrait;

    /**
     * @var HttpAsyncClient
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $requestLogger;

    /**
     * @var RequestFactory
     */
    private $requestFactory;
    /**
     * @var string
     */
    private $ICDN_URI;

    /**
     * I'm sorry for this shitty BASE_URI init through bind opt
     * But unfortunately I didn't catch how to pass the base_uri to client factory directly
     *
     * It could be much easier via guzzlehttp/guzzle native pack
     * But after the first try I decided to refactor this to use DI httplug with Guzzle adapter
     *
     * ICdnService constructor.
     * @param HttpAsyncClient $client
     * @param RequestFactory  $requestFactory
     * @param LoggerInterface $requestLogger
     * @param string          $ICDN_URI
     */
    public function __construct(HttpAsyncClient $client,
                                RequestFactory $requestFactory,
                                LoggerInterface $requestLogger,
                                string $ICDN_URI)
    {
        $this->client = $client;
        $this->logger = $requestLogger;
        $this->requestFactory = $requestFactory;
        $this->ICDN_URI = $ICDN_URI;
    }

    /**
     * @return array|null
     */
    public function getCategories(): ?array
    {
        $content = null;

        try {
            $request = $this->requestFactory->createRequest(
                'GET',
                $this->ICDN_URI . 'categories'
            );
            $promise = $this->client->sendAsyncRequest($request);

            /** @var ResponseInterface $result */
            $result = $promise->wait();
            $content = json_decode($result->getBody()->getContents());
            $this->checkResponseContent($content, $request);
        } catch (Exception $e) {
            $this->logMessage(LogLevel::ERROR, $e->getMessage(), [
                __METHOD__ => $this->client
            ]);

            return ['API service currently unavailable' => -1];
        }

        return array_flip($content->value);
    }

    /**
     * @param string|null $category
     * @return string|null
     */
    public function getJoke(?string $category): ?string
    {
        try {
            $request = $this->requestFactory->createRequest(
                'GET',
                $this->ICDN_URI . 'jokes/random/' . http_build_query(['limitTo' => $category])
            );
            $promise = $this->client->sendAsyncRequest($request);

            /** @var ResponseInterface $result */
            $result = $promise->wait();
            $randomJoke = json_decode($result->getBody()->getContents());
            $this->checkResponseContent($randomJoke, $request);

            if (!property_exists($randomJoke->value, 'joke')) {
                throw new UnexpectedValueException('joke is empty, API Joke generator is dead...');
            }

        } catch (Exception $e) {
            $this->logMessage(LogLevel::ERROR, $e->getMessage(), [
                __METHOD__ => $this->client
            ]);

            return null;
        }

        return $randomJoke->value->joke;
    }

    /**
     * @param stdClass         $content
     * @param RequestInterface $request
     * @throws BadResponseException
     * @throws UnexpectedValueException
     */
    private function checkResponseContent(stdClass $content, RequestInterface $request)
    {
        if (!property_exists($content, 'type') || $content->type !== 'success') {
            throw new BadResponseException('bad request', $request);
        }

        if (!property_exists($content, 'value')) {
            throw new UnexpectedValueException('no categories found');
        }
    }
}