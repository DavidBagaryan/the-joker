<?php

namespace App\Controller;

use App\Form\MailJokeFormType;
use App\Service\DataSaver;
use App\Service\EMailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RandomJokeController extends AbstractController
{
    /**
     * Hope this controller is not too fat
     *
     * @Route("/random-joke", name="random_joke")
     * @param Request   $request
     * @param DataSaver $dataSaver
     * @param EMailer   $eMailer
     * @return Response
     */
    public function index(Request $request, DataSaver $dataSaver, EMailer $eMailer)
    {
        $form = $this->createForm(MailJokeFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $mailJoke = $dataSaver->saveData($form->getData());

            if (!is_null($mailJoke) && $eMailer->send($mailJoke))
                $this->addFlash('success', ">> joke sent to {$mailJoke->getEmail()}");
            else
                $this->addFlash('error', ">> the joke email is not sent!");
        }

        return $this->render('random_joke/index.html.twig', [
            'mailJokeForm' => $form->createView()
        ]);
    }
}
