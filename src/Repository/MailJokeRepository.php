<?php

namespace App\Repository;

use App\Entity\MailJoke;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MailJoke|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailJoke|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailJoke[]    findAll()
 * @method MailJoke[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailJokeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MailJoke::class);
    }

    // /**
    //  * @return MailJoke[] Returns an array of MailJoke objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MailJoke
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
