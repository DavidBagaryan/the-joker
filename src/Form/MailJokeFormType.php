<?php

namespace App\Form;

use App\Entity\MailJoke;
use App\Service\ICdnService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailJokeFormType extends AbstractType
{
    /**
     * @var ICdnService
     */
    private $ICdnService;

    public function __construct(ICdnService $ICdnService)
    {
        $this->ICdnService = $ICdnService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email')
                ->add('category', ChoiceType::class, [
                    'choices' => $this->ICdnService->getCategories()
                ])
                ->add('submit', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MailJoke::class,
            'label'      => "Let's send a random joke!",
            'attr'       => ['id' => 'mail-joke-form']
        ]);
    }
}
