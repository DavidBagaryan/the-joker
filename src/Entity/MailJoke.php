<?php

namespace App\Entity;

use App\Helper\Mailable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use UnexpectedValueException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MailJokeRepository")
 */
class MailJoke implements Mailable
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Email(message = "The email {{ value }} is not valid")
     * @Assert\Length(max=255)
     * @Assert\NotNull()
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="smallint")
     */
    private $category;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCategory(): ?int
    {
        return $this->category;
    }

    public function setCategory(int $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        if (!$content) throw new UnexpectedValueException('content cannot be null!');

        $this->content = $content;
        return $this;
    }

    public function getSubject(): string
    {
        // todo translation if necessary
        // return "A random joke from {$this->category}";

        return "Случайная шутка из {$this->category}";
    }

    public function getDestination(): string
    {
        return $this->email;
    }

    public function getMessageContent(): ?string
    {
        return $this->content;
    }

    public function getFrom(): string
    {
        // could be moved to config
        return 'joker-service@lol.com';
    }
}
