<?php

namespace App\Helper;

interface Mailable
{
    public function getSubject(): string;

    public function getDestination(): string;

    public function getMessageContent(): ?string;

    public function getFrom(): string;
}