<?php

namespace App\Helper;

use Psr\Log\LoggerInterface;

/**
 * Could be reused further
 *
 * Trait LoggerTrait
 * @package App\Helper
 */
trait LoggerTrait
{
    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     * @required
     */
    public function setLogger(LoggerInterface $logger)
    {
        if (!$this->logger) $this->logger = $logger;
    }

    /**
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     */
    private function logMessage($level, string $message, array $context = [])
    {
        if ($this->logger) $this->logger->log($level, $message, $context);
    }
}