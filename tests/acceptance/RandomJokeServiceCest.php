<?php namespace App\Tests;

use App\Tests\AcceptanceTester;

/**
 * Class RandomJokeServiceCest
 * @group   random_joke_form
 * @package App\Tests
 */
class RandomJokeServiceCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function testRootPage(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->seeResponseCodeIs(404);
        $I->see('Welcome to', 'h1');
        $I->see('Symfony', 'h1');
    }

    public function testRandomJokePage(AcceptanceTester $I)
    {
        $I->amOnPage('/random-joke');
        $I->seeResponseCodeIsSuccessful();

        $I->see("Let's send a random joke!", 'label');

        $I->seeElement('form', [
            'name'   => 'mail_joke_form',
            'method' => 'post',
        ]);
        $I->seeElement('input', [
            'name' => 'mail_joke_form[email]'
        ]);
        $I->seeElement('select', [
            'name' => 'mail_joke_form[category]'
        ]);
        $I->seeElement('button', [
            'type' => 'submit'
        ]);
    }

    public function testFormInvalidEmail(AcceptanceTester $I)
    {
        $badEmail = 'bad@wrong';
        $I->amOnPage('/random-joke');
        $I->seeResponseCodeIsSuccessful();

        $I->submitForm('#mail-joke-form', [
            'mail_joke_form[email]' => $badEmail
        ], 'mail_joke_form[submit]');

        $I->see("The email \"{$badEmail}\" is not valid", 'li');
        $I->seeResponseCodeIsSuccessful();
    }

    /**
     * @group invalid_select
     * @param \App\Tests\AcceptanceTester $I
     */
    public function testFormInvalidSelect(AcceptanceTester $I)
    {
        $email = 'test@gmail.de';
        $I->amOnPage('/random-joke');
        $I->seeResponseCodeIsSuccessful();

        $I->submitForm('#mail-joke-form', [
            'mail_joke_form[email]'    => $email,
            'mail_joke_form[category]' => -1
        ], 'mail_joke_form[submit]');

        $I->see("This value is not valid.");
        $I->seeResponseCodeIsSuccessful();
    }

    /**
     * @group submits
     * @param \App\Tests\AcceptanceTester $I
     */
    public function testFormSubmits(AcceptanceTester $I)
    {
        $email = 'test@gmail.com';
        $I->amOnPage('/random-joke');
        $I->seeResponseCodeIsSuccessful();

        $I->submitForm('#mail-joke-form', [
            'mail_joke_form[email]'    => $email,
            'mail_joke_form[category]' => 0
        ], 'mail_joke_form[submit]');

        $I->see(">> joke sent to {$email}");
        $I->seeResponseCodeIsSuccessful();
        $I->canSeeInDatabase('mail_joke', ['email' => $email]);
    }
}
