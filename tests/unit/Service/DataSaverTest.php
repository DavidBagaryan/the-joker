<?php namespace App\Tests\Service;

use App\Entity\MailJoke;
use App\Service\DataSaver;
use App\Service\ICdnService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Class DataSaverTest
 * @group   data_saver
 * @package App\Tests\Service
 */
class DataSaverTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    /**
     * @var DataSaver
     */
    private $dataSaver;

    /**
     * @var FormInterface
     */
    private $form;

    private $testJokeStr = 'test joke';

    private $testEmail = 'test@gmail.de';

    protected function _before()
    {
        $mailJoke = new MailJoke();
        $mailJoke->setEmail($this->testEmail);
        $mailJoke->setCategory(0);

        $this->form = $this->createMock(FormInterface::class);
        $this->form->method('getData')
                   ->willReturn($mailJoke);

        $em = $this->createMock(EntityManagerInterface::class);

        $ICdnService = $this->createMock(ICdnService::class);
        $ICdnService->method('getJoke')
                    ->willReturn($this->testJokeStr);

        $this->dataSaver = new DataSaver($em, $ICdnService);

    }

    protected function _after()
    {
    }

    // tests
    public function testSavingData()
    {
        $this->tester->assertEquals(
            $this->testJokeStr,
            $this->dataSaver->saveData($this->form->getData())
                            ->getContent(),
            'jokes are not the same '
        );

        $this->tester->assertEquals(
            $this->testEmail,
            $this->dataSaver->saveData($this->form->getData())
                            ->getEmail(),
            'emails are not the same '
        );
    }

    public function testDataNotSaved()
    {
        $this->testEmail = 'wrong@mail.ru';

        $this->tester->assertNotEquals(
            'wrong content',
            $this->dataSaver->saveData($this->form->getData())
                            ->getContent(),
            'jokes are the same '
        );

        $this->tester->assertNotEquals(
            $this->testEmail,
            $this->dataSaver->saveData($this->form->getData())
                            ->getContent(),
            'emails are the same '
        );
    }
}