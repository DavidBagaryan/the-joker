<?php namespace App\Tests\Service;

use App\Entity\MailJoke;
use App\Helper\Mailable;
use App\Service\EMailer;
use Swift_Mailer;

class EMailerTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    /**
     * @var Mailable
     */
    private $mailable;

    protected function _before()
    {
        $this->mailable = new MailJoke();
        $this->mailable->setEmail('test@me.com');
        $this->mailable->setContent('test test test');
    }

    protected function _after()
    {
    }

    // tests
    public function testMailSent()
    {
        $swiftMailer = $this->createMock(Swift_Mailer::class);
        $swiftMailer->expects($this->once())
                    ->method('send')
                    ->willReturn(1);

        $eMailer = new EMailer($swiftMailer);
        $this->tester->assertTrue(
            $eMailer->send($this->mailable),
            'message is not sent'
        );
    }

    public function testMailNotSent()
    {
        $swiftMailer = $this->createMock(Swift_Mailer::class);
        $swiftMailer->expects($this->once())
                    ->method('send')
                    ->willReturn(0);

        $eMailer = new EMailer($swiftMailer);
        $this->tester->assertFalse(
            $eMailer->send($this->mailable),
            'message is not sent'
        );
    }
}