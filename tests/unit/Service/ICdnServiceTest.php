<?php namespace App\Tests\Service;

use App\Service\ICdnService;
use GuzzleHttp\Psr7\Request;
use Http\Client\HttpAsyncClient;
use Http\Message\RequestFactory;
use Http\Promise\Promise;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ICdnServiceTest
 * @group   ICDN_API
 * @package App\Tests\Service
 */
class ICdnServiceTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;

    /**
     * @var string
     */
    private $serviceResponse;

    /**
     * @var ICdnService
     */
    private $ICdnService;

    protected function _before()
    {

    }

    protected function _after()
    {
    }

    // tests
    public function testGetCategories()
    {
        $this->serviceResponse = '{ "type": "success", "value": [ "explicit", "nerdy" ] }';
        $this->prepareDataForGetCategories();
        $this->tester->assertTrue(
            $this->ICdnService->getCategories() === array_flip(json_decode($this->serviceResponse)->value),
            "can't get categories"
        );
    }

    public function testGetCategoriesNegative()
    {
        $this->serviceResponse = '{ "wrong": "wrong" }';
        $this->prepareDataForGetCategories();
        $this->tester->assertTrue(
            $this->ICdnService->getCategories() === ['API service currently unavailable' => -1],
            'API Response is not valid'
        );
    }

    public function testGetCategoriesEmpty()
    {
        $this->serviceResponse = '{}';
        $this->prepareDataForGetCategories();
        $this->tester->assertTrue(
            $this->ICdnService->getCategories() === ['API service currently unavailable' => -1],
            'API Response is not valid'
        );
    }

    public function testGetJoke()
    {
        $this->serviceResponse = '{ "type": "success", "value": { "id": 400, "joke": "Chuck Norris\' dick is so big, it has it\'s own dick, and that dick is still bigger than yours.", "categories": ["explicit"] } }';
        $this->prepareDataForGetCategories();
        $this->tester->assertTrue(
            $this->ICdnService->getJoke('any') === json_decode($this->serviceResponse)->value->joke,
            'jokes are not the same'
        );
    }

    public function testGetJokeNegative()
    {
        $this->serviceResponse = '{ "wrong": "wrong" }';
        $this->prepareDataForGetCategories();
        $this->tester->assertNull(
            $this->ICdnService->getJoke('any'),
            'API Response is not valid'
        );
    }

    public function testGetJokeEmpty()
    {
        $this->serviceResponse = '{}';
        $this->prepareDataForGetCategories();
        $this->tester->assertNull(
            $this->ICdnService->getJoke('any'),
            'API Response is not valid'
        );
    }

    private function prepareDataForGetCategories()
    {
        $serviceResponse = $this->serviceResponse;

        $stream = $this->createMock(StreamInterface::class);
        $stream->method('getContents')
               ->willReturn($serviceResponse);

        $response = $this->createMock(ResponseInterface::class);
        $response->method('getBody')
                 ->willReturn($stream);

        $promise = $this->getMockBuilder(Promise::class)
                        ->disableOriginalConstructor()
                        ->getMock();
        $promise->method('wait')
                ->willReturn($response);

        $client = $this->createMock(HttpAsyncClient::class);
        $client->method('sendAsyncRequest')
               ->willReturn($promise);

        $request = $this->getMockBuilder(Request::class)
                        ->disableOriginalConstructor()
                        ->getMock();

        $requestFactory = $this->createMock(RequestFactory::class);
        $requestFactory->method('createRequest')
                       ->willReturn($request);

        $logger = $this->createMock(LoggerInterface::class);
        $ICDN_URI = 'https://google.com';

        $this->ICdnService = new ICdnService($client, $requestFactory, $logger, $ICDN_URI);
    }
}