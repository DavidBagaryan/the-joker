to start the project:

1. `git clone` {protocol}://repo `your/dir/`
2. `cd your/dir/`
3. `composer install`
4. `bin/console migrate` (and Y both times)
5. `bin/console migrate --env=test` (the same Y both)
6. `bin/console server:start --env=test`
7. `vendor/bin/codecept run`

that's it, hope U'll enjoy